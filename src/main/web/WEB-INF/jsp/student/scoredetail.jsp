<%--
  Created by IntelliJ IDEA.
  User: fcsca
  Date: 2019-08-10/0010
  Time: 下午 17:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    pageContext.setAttribute("APP_PATH", request.getContextPath());
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <link href="<%=basePath%>bootstrap-4.1.3-dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="<%=basePath%>jquery-3.3.1/jquery-3.3.1.min.js"></script>
    <script src="<%=basePath%>js/page.js"></script>
    <base href="<%=basePath%>">
    <title>课程详情</title>
</head>
<body>

<div align="center" class="alert alert-success">
    <h3>LKD系统课程成绩页面</h3>
    <div align="right">
        <a href="user/sc?pageNumber=1&size=5&user.number=${course.score.stuNum}">返回我的课程</a>
    </div>
</div>

<div align="center">
    <table class="table table-dark">
        <thead>
        <tr>
            <th scope="col">课程号</th>
            <th scope="col">${course.num}</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row">课程名</th>
            <td>${course.name}</td>
        </tr>
        <tr>
            <th scope="row">上课地点</th>
            <td>${course.room}</td>
        </tr>
        <tr>
            <th scope="row">上课时间</th>
            <td>${course.startTime}-${course.endTime}</td>
        </tr>
        <tr>
            <th scope="row">上课教师</th>
            <td>${course.user.name}</td>
        </tr>
        <tr>
            <th scope="row">教师邮箱</th>
            <td>${course.user.email}</td>
        </tr>
        <tr>
            <th scope="row">教师电话</th>
            <td>${course.user.tel}</td>
        </tr>
        <tr>
            <th scope="row">课程成绩</th>
            <td>${course.score.score}</td>
        </tr>
        <tr>
            <th scope="row">选课时间</th>
            <td>${course.score.selectTime}</td>
        </tr>

        </tbody>
    </table>
</div>

</body>
</html>
