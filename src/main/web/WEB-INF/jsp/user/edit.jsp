<%--
  Created by IntelliJ IDEA.
  User: fcsca
  Date: 2019-08-07/0007
  Time: 下午 23:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    pageContext.setAttribute("APP_PATH", request.getContextPath());
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <link href="<%=basePath%>bootstrap-4.1.3-dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="<%=basePath%>jquery-3.3.1/jquery-3.3.1.min.js"></script>
    <base href="<%=basePath%>">
    <title>查看用户信息</title>
</head>
<body background="<%=basePath%>img/70.gif">

<div align="right">
    <a href="<%=basePath%>user/access?number=${user.number}">返回信息管理页</a>|
    <a href="<%=basePath%>user/signOut">Sign Out</a>
</div>

<div align="center" class="alert alert-success">
    <h2>LKD学生信息管理系统用户信息中心</h2>
</div>

<div align="center" class="text text-danger" style="margin-top: 30px">
    <h4>${msg}</h4>
</div>

<div class="container">
    <div align="center" style="margin-top: 30px; margin-left: auto">
        <form action="<%=basePath%>user/update" method="post">
            姓名<br/><input type="text" value="${user.name}" name="name"><br/>
            学号/工号<br/><input type="text" value="${user.number}" name="number" readonly><br/>
            手机<br/><input type="text" value="${user.tel}" name="tel"><br/>
            邮箱<br/><input type="text" value="${user.email}" name="email"><br/>
            密码<br/><input type="password" name="password"><br/>
            再次输入密码<br/><input type="password" name="repassword"><br/>
            学院<br/><input type="text" value="${user.college}" name="college" readonly><br/>
            地址<br/><input type="text" value="${user.address}" name="address"><br/><br/>
            <input type="reset" class="btn btn-warning" value="重置">
            <input type="submit" class="btn btn-primary" value="修改">
        </form>
    </div>
</div>

</body>
</html>
