package com.fcant.ssmcom.service;

import com.fcant.ssmcom.bean.Course;
import com.fcant.ssmcom.bean.Page;
import com.fcant.ssmcom.bean.Score;
import com.fcant.ssmcom.bean.User;

import java.util.List;
import java.util.Map;

/**
 * CourseService
 * <p>
 * encoding:UTF-8
 *
 * @author Fcscanf
 * @description
 * @date 上午 11:17 2019-08-09/0009
 */
public interface CourseService {

    /**
     * 查询选择课程的学生数量
     *
     * @return List<Map<Course, Integer>>
     * @author Fcant
     * @date 下午 15:21 2019-08-26/0026
     */
    Map<String, Integer> selectCourseStuNum();

    /**
     * 根据课程号查询课程详情
     *
     * @param courseNum 课程号
     * @return Course
     * @author Fcant
     * @date 下午 15:50 2019-08-11/0011
     */
    Course courseByCouNum(String courseNum);

    /**
     * 查询选择该课程的学生的总数
     *
     * @param courseNum 接口调用须为必要属性赋值：courseNum
     * @return List<Course> 将课程学生信息返回
     * @author Fcant
     * @date 上午 8:31 2019-08-11/0011
     */
    int selectCourseStudentByCouNumTotal(String courseNum);

    /**
     * 查询选择该课程的学生并返回这个学生课程成绩信息集合
     *
     * @param page 接口调用须为必要属性赋值：start、size、score.courseNum
     * @return List<Course> 将课程学生信息返回
     * @author Fcant
     * @date 上午 8:31 2019-08-11/0011
     */
    Course selectCourseStudentByCouNum(Page page);

    /**
     * 根据课程号和学号查询课程、成绩、教师信息
     *
     * @param score 接口调用须为必要属性赋值：courseNum、stuNum
     * @return Course
     * @author Fcscanf
     * @date 下午 17:16 2019-08-10/0010
     */
    Course courseByCouNumUserNum(Score score);

    /**
     * 分页查询所有课程
     *
     * @param page 接口调用须为必要属性赋值：start、size
     * @return List<Course>
     * @author Fcscanf
     * @date 上午 8:47 2019-08-10/0010
     */
    List<Course> allCoursePage(Page page);

    /**
     * 查询所有课程
     *
     * @return List<Course>
     * @author Fcscanf
     * @date 上午 8:47 2019-08-10/0010
     */
    List<Course> allCourse();

    /**
     * 根据教师号分页返回教师的课程
     *
     * @param page 接口调用须为必要属性赋值：start、size、user.number
     * @return List<Course>
     * @author Fcscanf
     * @date 下午 19:24 2019-08-09/0009
     */
    List<Course> queryPageByTid(Page page);

    /**
     * 根据学号查询选择的课程
     *
     * @param page 接口调用须为必要属性赋值：user.number
     * @return List<User>
     * @author Fcscanf
     * @date 下午 14:35 2019-08-09/0009
     */
    List<Course> queryCourseByNumberTotal(Page page);

    /**
     * 根据学号查询选择的课程
     *
     * @param page 接口调用须为必要属性赋值：start、size、user.number
     * @return List<User>
     * @author Fcscanf
     * @date 下午 14:35 2019-08-09/0009
     */
    List<Course> queryCourseByNumber(Page page);

    /**
     * 根据教师号查询课程
     *
     * @param user 接口调用须为必要属性赋值：number
     * @return List<Course>
     * @author Fcscanf
     * @date 上午 11:14 2019-08-09/0009
     */
    List<Course> selectByTid(User user);

}
