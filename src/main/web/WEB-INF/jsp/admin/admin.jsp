<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.fcant.ssmcom.bean.User" %><%--
  Created by IntelliJ IDEA.
  User: fcsca
  Date: 2019-08-06/0006
  Time: 上午 11:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <link href="<%=basePath%>bootstrap-4.1.3-dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="<%=basePath%>jquery-3.3.1/jquery-3.3.1.min.js"></script>
    <script src="<%=basePath%>js/page.js"></script>
    <base href="<%=basePath%>">
    <title>LKD系统管理员页面</title>
</head>
<body background="<%=basePath%>img/153.gif">

<div align="right" style="margin-right: 10px">
    <%
        User user = (User) request.getSession().getAttribute("user");
    %>
    欢迎管理员<%=user.getName()%>登录
    <a href="<%=basePath%>user/detail?number=<%=user.getNumber()%>">查看个人信息</a>|
    <a href="<%=basePath%>user/signOut">Sign Out</a>
</div>

<%--页面标题设计--%>
<div align="center" class="alert alert-dark">
    <h3>LKD系统管理员页面</h3>
</div>

<%--功能框设计--%>
<div align="center" style="margin-top: 30px">
    <div class="btn-group mr-2">
        <select id="type">
            <option value="name" id="name">姓名</option>
            <option value="number" id="number" selected>学号/工号</option>
            <option value="email" id="email">邮箱</option>
            <option value="tel" id="tel">手机</option>
            <option value="college" id="college">学院</option>
            <option value="address" id="address">地址</option>
        </select>
        <select id="role">
            <option value="1" id="stu" selected>学生</option>
            <option value="2" id="teacher">教师</option>
        </select>
        <input id="key" onfocus="javascript:if(this.value=='请输入查询的内容')this.value='';">
        <button onclick="likeQuery(1)" class="btn btn-secondary">检索</button>&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <button onclick="sendRequest('<%=basePath%>page/add', '')" class="btn btn-primary">添加教师/学生信息</button>
    <button class="btn btn-success">批量添加教师/学生信息-Excel导入</button>
    <button href="" onclick="toCharts()" class="btn btn-warning">图表分析选课情况</button>
</div>

<%--显示用户信息--%>
<div class="row">
    <div class="col-md-12">
        <table class="table table-hover">
            <tr>
                <th>学号/工号</th>
                <th>姓名</th>
                <th>手机</th>
                <th>邮箱</th>
                <th>学院</th>
                <th>地址</th>
                <th>操作</th>
            </tr>
            <c:forEach items="${page.data}" var="user">
                <tr>
                    <th>${user.number}</th>
                    <th>${user.name}</th>
                    <th>${user.tel}</th>
                    <th>${user.email}</th>
                    <th>${user.college}</th>
                    <th>${user.address}</th>
                    <th>
                        <button class="btn btn-success btn-sm">
                            Edit<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                        </button>
                        <button class="btn btn-danger btn-sm">
                            Delete<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                        </button>
                    </th>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>

<%--显示分页的信息--%>
<div class="row">
    <%--分页文字信息--%>
    <div class="col-md-7">当前第${page.pageNumber}页|共有${page.setTolPageNum()}页|共有${page.total}条记录
    </div>
    <%--分页条信息--%>
    <div class="col-md-5">
        <div align="right">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item">
                        每页显示<input id="size" style="width: 10%;" value="${page.size}">条数据
                    </li>
                    <li class="page-item">
                        <a class="page-link" aria-label="Previous">
                            <span aria-hidden="true" onclick="likeQuery(1)">首页</span>
                        </a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" onclick="likeQueryPro(${page.pageNumber})" aria-label="Previous">
                            <span aria-hidden="true" onclick="">&laquo;</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" onclick="likeQuery(${page.pageNumber})">${page.pageNumber}</a></li>
<%--                    <li class="page-item"><a class="page-link" href="#">2</a></li>--%>
<%--                    <li class="page-item"><a class="page-link" href="#">3</a></li>--%>
                    <li class="page-item">
                        <a class="page-link" onclick="likeQueryNext(${page.pageNumber})" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" aria-label="Next">
                            <span aria-hidden="true" onclick="likeQuery(${page.setTolPageNum()})">尾页</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>

</body>
<script>

    function toCharts() {
        location.href = "<%=basePath%>page/charts";
    }

    $(function () {
        // likeQuery();
    });

    /**
     * 设置分页的默认值
     *
     * @return size
     * @author Fcscanf
     * @date 下午 13:16 2019-08-10/0010
     */
    function defaultSize() {
        var size = document.getElementById("size").value;
        if (size == "" || size.length == 0) {
            size = 6;
            return size;
        }else {
            return size;
        }
    }

    /**
     * 模糊查询用户
     *
     * @param pN 当前页数
     * @author Fcscanf
     * @date 下午 13:16 2019-08-10/0010
     */
    // 模糊查询用户
    function likeQuery(pN) {
        var size = defaultSize();
        var key = document.getElementById("key").value;
        var type = $("#type").val();
        var role = $("#role").val();
        location.href="<%=basePath%>user/page?pageNumber="+pN+"&size="+size+"&rId="+
            role+"&type="+type+"&key="+key;
    }

    /**
     * 上一页
     *
     * @param pN 当前页数
     * @return
     * @author Fcscanf
     * @date 下午 13:17 2019-08-10/0010
     */
    <!--上一页-->
    function likeQueryPro(pN) {
        if (pN != 1) {
            pN = pN -1
        }
        var size = defaultSize();
        var key = document.getElementById("key").value;
        var type = $("#type").val();
        var role = $("#role").val();
        location.href="<%=basePath%>user/page?pageNumber="+pN+"&size="+size+"&rId="+
            role+"&type="+type+"&key="+key;
    }

    /**
     * 下一页
     *
     * @param pN 当前页数
     * @return
     * @author Fcscanf
     * @date 下午 13:17 2019-08-10/0010
     */
    <!--下一页-->
    function likeQueryNext(pN) {
        var size = defaultSize();
        if (pN == ${page.setTolPageNum()}) {
            pN =
            ${page.setTolPageNum()}
        } else {
            pN = pN +1;
        }
        var key = document.getElementById("key").value;
        var type = $("#type").val();
        var role = $("#role").val();
        location.href="<%=basePath%>user/page?pageNumber="+pN+"&size="+size+"&rId="+
            role+"&type="+type+"&key="+key;
    }

</script>
</html>
