<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: fcsca
  Date: 2019-08-10/0010
  Time: 上午 9:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    pageContext.setAttribute("APP_PATH", request.getContextPath());
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <link href="<%=basePath%>bootstrap-4.1.3-dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="<%=basePath%>jquery-3.3.1/jquery-3.3.1.min.js"></script>
    <script src="<%=basePath%>js/page.js"></script>
    <base href="<%=basePath%>">
    <title>LKD系统选课清单</title>
</head>
<body>
<div align="center" class="alert alert-primary">
    <h3>LKD系统选课课程清单</h3>
    <div align="right">
        <a href="user/sc?pageNumber=1&size=5&user.number=${page.user.number}">返回我的课程</a>
    </div>
</div>

<%--课程内容展示--%>
<div align="center">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover">
                <tr>
                    <th>课程号</th>
                    <th>课程名</th>
                    <th>教室</th>
                    <th>上课时间</th>
                    <th>结束时间</th>
                    <th>操作</th>
                </tr>
                <c:forEach items="${page.data}" var="course">
                    <tr>
                        <th>${course.num}</th>
                        <th>${course.name}</th>
                        <th>${course.room}</th>
                        <th>${course.startTime}</th>
                        <th>${course.endTime}</th>
                        <th>
                            <button onclick="addCourse('<%=basePath%>', ${page.user.number}, ${course.num}, '${course.name}')" class="btn btn-danger btn-sm">
                                选择课程<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                            </button>
                            <button onclick="sendRequest('<%=basePath%>course/cd', 'pageNumber=1&size=5&score.courseNum=${course.num}&user.number=${page.user.number}')" class="btn btn-success btn-sm">
                                课程详情<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                            </button>
                        </th>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
</div>

<%--显示分页的信息--%>
<div class="row">
    <%--分页文字信息--%>
    <div class="col-md-7">当前第${page.pageNumber}页|共有${page.setTolPageNum()}页|共有${page.total}条记录
    </div>
    <%--分页条信息--%>
    <div class="col-md-5">
        <div align="right">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item">
                        每页显示<input id="size" style="width: 10%;" value="${page.size}">条数据
                    </li>
                    <li class="page-item">
                        <a class="page-link" aria-label="Previous">
                            <span aria-hidden="true" onclick="pageQuery('<%=basePath%>course/all', 1, 'user.number=${page.user.number}')">首页</span>
                        </a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" onclick="pageQueryPro('<%=basePath%>course/all', ${page.pageNumber}, 'user.number=${page.user.number}')" aria-label="Previous">
                            <span aria-hidden="true" onclick="">&laquo;</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" onclick="pageQuery('<%=basePath%>course/all', ${page.pageNumber}, 'user.number=${page.user.number}')">${page.pageNumber}</a></li>
                    <%--                    <li class="page-item"><a class="page-link" href="#">2</a></li>--%>
                    <%--                    <li class="page-item"><a class="page-link" href="#">3</a></li>--%>
                    <li class="page-item">
                        <a class="page-link" onclick="pageQueryNext('<%=basePath%>course/all', ${page.pageNumber}, ${page.setTolPageNum()} , 'user.number=${page.user.number}')" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" aria-label="Next">
                            <span aria-hidden="true" onclick="pageQuery('<%=basePath%>course/all', ${page.setTolPageNum()}, 'user.number=${page.user.number}')">尾页</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>

</body>

<script>

</script>

</html>
