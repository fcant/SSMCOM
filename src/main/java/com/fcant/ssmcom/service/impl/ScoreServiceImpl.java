package com.fcant.ssmcom.service.impl;

import com.fcant.ssmcom.bean.Course;
import com.fcant.ssmcom.bean.Score;
import com.fcant.ssmcom.bean.User;
import com.fcant.ssmcom.mapper.ScoreMapper;
import com.fcant.ssmcom.service.ScoreService;
import com.fcant.ssmcom.utils.FcantUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * ScoreServiceImpl
 * <p>
 * encoding:UTF-8
 *
 * @author Fcscanf
 * @description
 * @date 下午 14:06 2019-08-09/0009
 */
@Service
public class ScoreServiceImpl implements ScoreService {

    @Autowired
    ScoreMapper scoreMapper;

    /**
     * 添加成绩
     *
     * @param score 接口调用时为必要属性赋值：scId、score
     * @return int
     * @author Fcant
     * @date 上午 8:12 2019-08-12/0012
     */
    @Override
    public int updateByPrimaryKey(Score score) {
        if (score == null || "".equals(score)) {
            return 0;
        }else {
            return scoreMapper.updateByPrimaryKey(score);
        }
    }

    /**
     * 根据课程号和学号查询当前选课是否存在
     *
     * @param score 调用接口时须为必要属性赋值：stuNum、courseNum
     * @return Score
     * @author Fcant
     * @date 下午 23:46 2019-08-11/0011
     */
    @Override
    public Score selectCourseByCouNumStuNum(Score score) {
        return scoreMapper.selectCourseByCouNumStuNum(score);
    }

    /**
     * 选课
     *
     * @param score 接口调用须为必要属性赋值：stuNum、courseNum
     * @throws ParseException 抛出异常
     * @return int
     * @author Fcscanf
     * @date 下午 12:33 2019-08-10/0010
     */
    @Override
    public int insert(Score score) throws ParseException {
        Score selectCourseByCouNumStuNum = scoreMapper.selectCourseByCouNumStuNum(score);
        if (selectCourseByCouNumStuNum == null) {
            score.setSelectTime(FcantUtils.formatDate(new Date()));
            return scoreMapper.insert(score);
        }else {
            return 5;
        }

    }

    /**
     * 退课
     *
     * @param score 接口调用须为必要属性赋值：stuNum、courseNum
     * @return int
     * @author Fcscanf
     * @date 下午 20:21 2019-08-09/0009
     */
    @Override
    public int delStuCourse(Score score) {
        return scoreMapper.delStuCourse(score);
    }

    /**
     * 根据课程号查询学号
     *
     * @param course 接口调用须为必要属性赋值：num
     * @return List<Score>
     * @author Fcscanf
     * @date 下午 13:58 2019-08-09/0009
     */
    @Override
    public List<Score> queryByCourseNum(Course course) {
        return scoreMapper.queryByCourseNum(course);
    }

    /**
     * 根据学号查询选课成绩表
     *
     * @param user 接口调用须为必要属性赋值：number
     * @return List<Score>
     * @author Fcscanf
     * @date 下午 13:56 2019-08-09/0009
     */
    @Override
    public List<Score> queryByNumber(User user) {
        return scoreMapper.queryByNumber(user);
    }
}
