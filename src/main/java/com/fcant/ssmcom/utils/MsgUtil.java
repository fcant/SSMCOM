package com.fcant.ssmcom.utils;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * MsgUtils
 * <p>
 * encoding:UTF-8
 *
 * @author Fcant
 * @date 下午 18:05 2019-08-11/0011
 */
@Data
public class MsgUtil {

    /**
     * 状态码 100-成功 200-失败
     */
    private int code;

    /**
     * 提示信息
     */
    private String msg;

    /**
     * 用户返回给浏览器的数据
     */
    private Map<String, Object> result = new HashMap<String, Object>();

    public static MsgUtil success() {
        MsgUtil msgUtil = new MsgUtil();
        msgUtil.setCode(100);
        msgUtil.setMsg("操作成功!");
        return msgUtil;
    }

    public static MsgUtil fail() {
        MsgUtil msgUtil = new MsgUtil();
        msgUtil.setCode(200);
        msgUtil.setMsg("操作失败!");
        return msgUtil;
    }

    public MsgUtil add(String key, Object value) {
        this.getResult().put(key, value);
        return this;
    }

}
