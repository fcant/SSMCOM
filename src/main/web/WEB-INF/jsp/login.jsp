<%--
  Created by IntelliJ IDEA.
  User: fcsca
  Date: 2019-08-02/0002
  Time: 上午 10:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <link href="<%=basePath%>bootstrap-4.1.3-dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="<%=basePath%>jquery-3.3.1/jquery-3.3.1.min.js"></script>
    <base href="<%=basePath%>">
    <title>LKD学生信息管理系统首页</title>
</head>
<body background="<%=basePath%>img/96.jpg">
<div align="center" class="alert alert-warning">
    <h2>LKD系统用户登录</h2>
</div>

<div class="alert alert-success" align="center" style="margin-top: 30px">
    <h4>${msg}</h4>
</div>

<div style="margin-top: 50px" align="center">
    <form action="<%=basePath%>user/login" method="post">
        工号/学号<input type="text" name="number"><br/><br/>
        &nbsp;&nbsp;密&nbsp;&nbsp;&nbsp;&nbsp;码&nbsp;&nbsp;
        <input type="password" name="password"><br/><br/>
        <input class="btn btn-warning" type="reset" value="重置">
        <input class="btn btn-success" type="submit" value="登录">
    </form>
</div>
</body>
</html>
