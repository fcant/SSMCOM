package com.fcant.ssmcom.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * PageController
 * <p>
 * encoding:UTF-8
 *
 * @author Fcscanf
 * @description
 * @date 上午 9:45 2019-08-07/0007
 */
@Controller
@RequestMapping("/page")
public class PageController {

    @RequestMapping("/charts")
    String getChartsPages() {
        return "admin/charts";
    }

    /**
     * 用于登录跳转
     *
     * @author Fcscanf
     * @date 下午 13:38 2019-08-10/0010
     */
    @RequestMapping("/login")
    public String getLoginPage() {
        return "login";
    }

    @RequestMapping("/add")
    public String getAddUserPage() {
        return "user/adduser";
    }
}
