package com.fcant.ssmcom.test;
import com.fcant.ssmcom.bean.Course;
import com.fcant.ssmcom.bean.Page;
import com.fcant.ssmcom.bean.Score;
import com.fcant.ssmcom.bean.User;
import com.fcant.ssmcom.service.CourseService;
import com.fcant.ssmcom.service.ScoreService;
import com.fcant.ssmcom.service.UserService;
import com.github.pagehelper.PageInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * MapperTest
 * <p>
 * encoding:UTF-8
 *
 * @author Fcscanf
 * @description
 * @date 下午 19:13 2019-08-06/0006
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-mybatis.xml")
public class MapperTest {

    @Autowired
    UserService userService;

    @Autowired
    CourseService courseService;

    @Autowired
    ScoreService scoreService;

    User user = new User();

    Score score = new Score();

    Page page = new Page();

    @Test
    public void courseByCourseNumTest() {
        System.out.println(courseService.courseByCouNum("9000000112"));
    }

    @Test
    public void courseScoreListStuTest() {
        score.setCourseNum("9000000037");
        page.setScore(score);
        page.setSize(5);
        Course course = courseService.selectCourseStudentByCouNum(page);
        System.out.println(course);
        System.out.println("总数为：" + courseService.selectCourseStudentByCouNumTotal(score.getCourseNum()));
    }

    @Test
    public void courseScoreTeaTest() {
        score.setStuNum("1610701106");
        score.setCourseNum("9000000015");
        Course course = courseService.courseByCouNumUserNum(score);
        System.out.println(course);
    }

    @Test
    public void allCourseTest() {
        List<Course> courses = courseService.allCourse();
        System.out.println("所有课程信息如下：");
        for (Course course : courses) {
            System.out.println(course);
        }
        System.out.println("分页后前五条数据：");
        PageInfo pageInfo = new PageInfo(courses);
        pageInfo.setSize(5);
        System.out.println("一共有" + pageInfo.getTotal());
        for (Object o : pageInfo.getList()) {
            System.out.println(o);
        }
    }

    @Test
    public void courseByNumberTest() {
        page.setSize(6);
        user.setNumber("1610701106");
        page.setUser(user);
        List<Course> courses = courseService.queryCourseByNumber(page);
        for (Course course : courses) {
            System.out.println(course);
        }
    }

    @Test
    public void scoreByCourseNumberTest() {
        Course course = new Course();
        course.setNum("9000000030");
        List<Score> scores = scoreService.queryByCourseNum(course);
        for (Score score : scores) {
            System.out.println(score);
        }
    }

    @Test
    public void scoreByNumberTest() {
        user.setNumber("1610701106");
        List<Score> scores = scoreService.queryByNumber(user);
        for (Score score : scores) {
            System.out.println(score);
        }
    }

    @Test
    public void courseByTidTest() {
        User user = new User();
        user.setNumber("1610701110");
        List<Course> courses = courseService.selectByTid(user);
        for (Course course : courses) {
            System.out.println(course);
        }
    }

    @Test
    public void tableTotalTest() {
        System.out.println(userService.tableTotal());
    }

    @Test
    public void addUserTest() {
        User user = new User();
        user.setName("樊乘乘");
        user.setNumber("1610701107");
        user.setTel("17826260016");
        user.setEmail("fcscanf@outlook.com");
        user.setPassword("111");
        user.setAddress("北京市太和区");
        user.setCollege("信息学院");
        user.setIsvalid(1);
        user.setrId(1);
        User selectByNumber = userService.selectByNumber(user);
        System.out.println(selectByNumber);
    }

    @Test
    public void updateUserTest() {
        User user = new User();
        user.setName("樊乘乘");
        user.setNumber("121");
        user.setTel("17826260016");
        user.setEmail("fcscanf@gmail.com");
        user.setPassword("111222");
        user.setAddress("北京市朝阳区");
        user.setrId(1);
        userService.updateByNumber(user);
    }

    @Test
    public void likePageLikeTest() {
        User user = new User();
//        user.setName("F");
//        user.setNumber("121");
//        user.setTel("17826260016");
//        user.setEmail("fcscanf@gmail.com");
//        user.setPassword("111222");
//        user.setAddress("北京市朝阳区");
        user.setrId(2);
        Page page = new Page();
        page.setSize(5);
        page.setStart(0);
        page.setUser(user);
        List<User> users = userService.queryUserLikePage(page);
        for (User u : users) {
            System.out.println(u);
        }
    }
}
