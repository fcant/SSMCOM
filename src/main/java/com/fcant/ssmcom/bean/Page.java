package com.fcant.ssmcom.bean;

import lombok.Data;

import java.util.List;

/**
 * Page
 * <p>
 * encoding:UTF-8
 *
 * @author Fcscanf
 * @description
 * @date 下午 14:05 2019-08-08/0008
 */
@Data
public class Page<T> {

    /**
     * 分页大小
     */
    private int size;

    /**
     * 分页起始查询
     */
    private int start;

    /**
     * 当前分页的最后一条-非总条数的最后一条
     */
    private int end;

    /**
     * 总条数
     */
    private int total;

    /**
     * 页数
     */
    private int pageNumber;

    private User user;

    private Score score;

    private Course course;

    /**
     * 分页封装的数据
     */
    private List<T> data;

    public int setTolPageNum() {
        int i = total / size;
        int y = total % size;
        if (y != 0) {
            return i + 1;
        }
        return i;
    }
}
