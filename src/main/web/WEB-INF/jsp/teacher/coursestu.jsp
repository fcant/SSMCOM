<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: fcsca
  Date: 2019-08-11/0011
  Time: 上午 9:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    pageContext.setAttribute("APP_PATH", request.getContextPath());
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <link href="<%=basePath%>bootstrap-4.1.3-dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="<%=basePath%>jquery-3.3.1/jquery-3.3.1.min.js"></script>
    <script src="<%=basePath%>js/page.js"></script>
    <base href="<%=basePath%>">
    <title>课程学生管理</title>
</head>
<body>

<div align="center" class="alert alert-success">
    <h3>LKD系统课程学生管理</h3>
    <div align="right">
        <a href="user/tc?pageNumber=1&size=5&user.number=${page.course.user.number}">返回我的课程</a>
    </div>
</div>

<table class="table table-dark">
    <thead>
    <tr class="bg-primary">
        <th scope="col">课程号</th>
        <th scope="col">${page.course.num}</th>
        <th scope="col">课程名</th>
        <th scope="col">${page.course.name}</th>
        <th scope="col">选课人数</th>
        <th scope="col">${page.total}</th>
        <th scope="col">上课地点</th>
        <th scope="col">${page.course.room}</th>
    </tr>
    </thead>
    <thead>
    <tr>
        <th scope="col">学号</th>
        <th scope="col">姓名</th>
        <th scope="col">学院</th>
        <th scope="col">手机</th>
        <th scope="col">邮箱</th>
        <th scope="col">地址</th>
        <th scope="col">成绩</th>
        <th scope="col">操作</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${page.data}" var="stu">
        <tr>
            <th>${stu.number}</th>
            <th>${stu.name}</th>
            <th>${stu.college}</th>
            <th>${stu.tel}</th>
            <th>${stu.email}</th>
            <th>${stu.address}</th>
            <th><input value="${stu.score.score}" id="${stu.number}score" style="width: 20%"></th>
            <th>
                <button onclick="stuScore('<%=basePath%>course/score?scId=${stu.score.scId}&score=', ${stu.number})" class="btn btn-danger btn-sm">
                    打分<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                </button>
                <button class="btn btn-success btn-sm">
                    查看选课学生<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                </button>
            </th>
        </tr>
    </c:forEach>
    </tbody>
</table>

<%--显示分页的信息--%>
<div class="row">
    <%--分页文字信息--%>
    <div class="col-md-7">当前第${page.pageNumber}页|共有${page.setTolPageNum()}页|共有${page.total}条记录
    </div>
    <%--分页条信息--%>
    <div class="col-md-5">
        <div align="right">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item">
                        每页显示<input id="size" style="width: 10%;" value="${page.size}">条数据
                    </li>
                    <li class="page-item">
                        <a class="page-link" aria-label="Previous">
                            <span aria-hidden="true"
                                  onclick="pageQuery('<%=basePath%>user/cs', 1, 'score.courseNum=${page.score.courseNum}')">首页</span>
                        </a>
                    </li>
                    <li class="page-item">
                        <a class="page-link"
                           onclick="pageQueryPro('<%=basePath%>user/cs', ${page.pageNumber}, 'score.courseNum=${page.score.courseNum}')"
                           aria-label="Previous">
                            <span aria-hidden="true" onclick="">&laquo;</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link"
                                             onclick="pageQuery('<%=basePath%>user/tc', ${page.pageNumber}, 'score.courseNum=${page.score.courseNum}')">${page.pageNumber}</a>
                    </li>
                    <%--                    <li class="page-item"><a class="page-link" href="#">2</a></li>--%>
                    <%--                    <li class="page-item"><a class="page-link" href="#">3</a></li>--%>
                    <li class="page-item">
                        <a class="page-link"
                           onclick="pageQueryNext('<%=basePath%>user/cs', ${page.pageNumber}, ${page.setTolPageNum()} , 'score.courseNum=${page.score.courseNum}')"
                           aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" aria-label="Next">
                            <span aria-hidden="true"
                                  onclick="pageQuery('<%=basePath%>user/cs', ${page.setTolPageNum()}, 'score.courseNum=${page.score.courseNum}')">尾页</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>

</body>
</html>
