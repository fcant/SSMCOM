<%@ page import="com.fcant.ssmcom.bean.User" %><%--
  Created by IntelliJ IDEA.
  User: fcsca
  Date: 2019-08-11/0011
  Time: 下午 17:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    pageContext.setAttribute("APP_PATH", request.getContextPath());
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <link href="<%=basePath%>bootstrap-4.1.3-dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="<%=basePath%>jquery-3.3.1/jquery-3.3.1.min.js"></script>
    <script src="<%=basePath%>js/page.js"></script>
    <base href="<%=basePath%>">
    <title>添加新用户</title>
</head>
<body>

<div align="center" class="alert alert-primary">
    <%
        User user = (User) request.getSession().getAttribute("user");
    %>
    <h3>LKD系统添加新用户</h3>
    <div align="right">
        <a href="user/access?number=<%=user.getNumber()%>">返回</a>
    </div>
</div>

<div>
    <table width="80px" border="1px" align="center" cellpadding="0"
           cellspacing="0" class="table table-secondary">
        <tr align="center">
            <td width="50%">学号</td>
            <td width="50%"><input id="number"/></td>
        </tr>
        <tr align="center">
            <td width="50%">姓名</td>
            <td width="50%"><input id="name"/></td>
        </tr>
        <tr align="center">
            <td width="50%">手机</td>
            <td width="50%"><input id="tel"/></td>
        </tr>
        <tr align="center">
            <td width="50%">邮箱</td>
            <td width="50%"><input id="email"/></td>
        </tr>
        <tr align="center">
            <td width="50%">学院</td>
            <td width="50%"><input id="college"/></td>
        </tr>
        <tr align="center">
            <td width="50%">
                身份
            </td>
            <td width="50%">
                <select name="rId">
                    <option value="1" selected>学生</option>
                    <option value="2">教师</option>
                </select>
            </td>

        </tr>
        <tr align="center">
            <td width="50%"><input class="btn-danger" type="reset" value="重置"></td>
            <td width="50%"><input class="btn-info" onclick="addUser('<%=basePath%>', 'user/add', 'number=<%=user.getNumber()%>')" type="button" value="添加" id="update"></td>
        </tr>
    </table>
</div>

</body>

<script>

</script>
</html>
