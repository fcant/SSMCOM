package com.fcant.ssmcom.service.impl;

import com.fcant.ssmcom.bean.Course;
import com.fcant.ssmcom.bean.Page;
import com.fcant.ssmcom.bean.Score;
import com.fcant.ssmcom.bean.User;
import com.fcant.ssmcom.mapper.CourseMapper;
import com.fcant.ssmcom.mapper.ScoreMapper;
import com.fcant.ssmcom.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * CourseServiceImpl
 * <p>
 * encoding:UTF-8
 *
 * @author Fcscanf
 * @description
 * @date 上午 11:17 2019-08-09/0009
 */
@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    CourseMapper courseMapper;

    @Autowired
    ScoreMapper scoreMapper;

    /**
     * 查询选择课程的学生数量
     *
     * @return List<Map < Course, Integer>>
     * @author Fcant
     * @date 下午 15:21 2019-08-26/0026
     */
    @Override
    public Map<String, Integer> selectCourseStuNum() {
        Map<String, Integer> map = new HashMap<>(500);
        List<Course> courses = courseMapper.allCourse();
        for (Course course : courses) {
            List<Score> scores = scoreMapper.queryByCourseNum(course);
            map.put(course.getName(), scores.size());
        }
        return map;
    }

    /**
     * 根据课程号查询课程详情
     *
     * @param courseNum 课程号
     * @return Course
     * @author Fcant
     * @date 下午 15:50 2019-08-11/0011
     */
    @Override
    public Course courseByCouNum(String courseNum) {
        return courseMapper.courseByCouNum(courseNum);
    }

    /**
     * 查询选择该课程的学生的总数
     *
     * @param courseNum 接口调用须为必要属性赋值：courseNum
     * @return List<Course> 将课程学生信息返回
     * @author Fcant
     * @date 上午 8:31 2019-08-11/0011
     */
    @Override
    public int selectCourseStudentByCouNumTotal(String courseNum) {
        return courseMapper.selectCourseStudentByCouNumTotal(courseNum);
    }

    /**
     * 查询选择该课程的学生并返回这个学生课程成绩信息集合
     *
     * @param page 接口调用须为必要属性赋值：start、size、score.courseNum
     * @return List<Course> 将课程学生信息返回
     * @author Fcant
     * @date 上午 8:31 2019-08-11/0011
     */
    @Override
    public Course selectCourseStudentByCouNum(Page page) {
        return courseMapper.selectCourseStudentByCouNum(page);
    }

    /**
     * 根据课程号和学号查询课程、成绩、教师信息
     *
     * @param score 接口调用须为必要属性赋值：courseNum、stuNum
     * @return Course
     * @author Fcscanf
     * @date 下午 17:16 2019-08-10/0010
     */
    @Override
    public Course courseByCouNumUserNum(Score score) {
        return courseMapper.courseByCouNumUserNum(score);
    }

    /**
     * 分页查询所有课程
     *
     * @param page 接口调用须为必要属性赋值：start、size
     * @return List<Course>
     * @author Fcscanf
     * @date 上午 8:47 2019-08-10/0010
     */
    @Override
    public List<Course> allCoursePage(Page page) {
        return courseMapper.allCoursePage(page);
    }

    /**
     * 查询所有课程
     *
     * @return List<Course>
     * @author Fcscanf
     * @date 上午 8:47 2019-08-10/0010
     */
    @Override
    public List<Course> allCourse() {
        return courseMapper.allCourse();
    }

    /**
     * 根据教师号分页返回教师的课程
     *
     * @param page 接口调用须为必要属性赋值：start、size、user.number
     * @return List<Course>
     * @author Fcscanf
     * @date 下午 19:24 2019-08-09/0009
     */
    @Override
    public List<Course> queryPageByTid(Page page) {
        return courseMapper.queryPageByTid(page);
    }

    /**
     * 根据学号查询选择的课程
     *
     * @param page 接口调用须为必要属性赋值：user.number
     * @return List<User>
     * @author Fcscanf
     * @date 下午 14:35 2019-08-09/0009
     */
    @Override
    public List<Course> queryCourseByNumberTotal(Page page) {
        return courseMapper.queryCourseByNumberTotal(page);
    }

    /**
     * 根据学号查询选择的课程
     *
     * @param page 接口调用须为必要属性赋值：start、size、user.number
     * @return List<User>
     * @author Fcscanf
     * @date 下午 14:35 2019-08-09/0009
     */
    @Override
    public List<Course> queryCourseByNumber(Page page) {
        return courseMapper.queryCourseByNumber(page);
    }

    /**
     * 根据教师号查询课程
     *
     * @param user 接口调用须为必要属性赋值：number
     * @return List<Course>
     * @author Fcscanf
     * @date 上午 11:14 2019-08-09/0009
     */
    @Override
    public List<Course> selectByTid(User user) {
        return courseMapper.selectByTid(user);
    }
}
