package com.fcant.ssmcom.controller;

import com.fcant.ssmcom.bean.Course;
import com.fcant.ssmcom.bean.Page;
import com.fcant.ssmcom.bean.Score;
import com.fcant.ssmcom.bean.User;
import com.fcant.ssmcom.service.CourseService;
import com.fcant.ssmcom.service.ScoreService;
import com.fcant.ssmcom.service.UserService;
import com.fcant.ssmcom.utils.FcantUtils;
import com.fcant.ssmcom.utils.MsgUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.text.ParseException;
import java.util.Map;

/**
 * CourseController
 * <p>
 * encoding:UTF-8
 *
 * @author Fcscanf
 * @description
 * @date 下午 23:16 2019-08-09/0009
 */
@Controller
@RequestMapping("/course")
public class CourseController {

    @Autowired
    CourseService courseService;

    @Autowired
    ScoreService scoreService;

    @Autowired
    UserService userService;

    @ResponseBody
    @RequestMapping("/coursenum")
    Map selectCourseStuNum() {
        return courseService.selectCourseStuNum();
    }

    /**
     * 为当前课程添加成绩
     *
     * @param score 接口调用时为必要属性赋值：scId、score
     * @return MsgUtil
     * @author Fcant
     * @date 上午 8:17 2019-08-12/0012
     */
    @ResponseBody
    @RequestMapping("/score")
    public MsgUtil addScore(Score score) {
        if (scoreService.updateByPrimaryKey(score) == 1) {
            return MsgUtil.success();
        }else {
            return MsgUtil.fail();
        }
    }

    /**
     * 根据课程号查询选择课程的学生和学生成绩以及课程信息
     *
     * @param page 接口调用须为必要属性赋值：score.courseNum、pageNumber、size
     * @return ModelAndView
     * @author Fcant
     * @date 上午 8:51 2019-08-11/0011
     */
    @RequestMapping("/cd")
    public ModelAndView selectListStuCourseByCouNum(Page page) {
        ModelAndView modelAndView = new ModelAndView();
        page = FcantUtils.setPageManager(page);
        page.setTotal(courseService.selectCourseStudentByCouNumTotal(page.getScore().getCourseNum()));
        Course course = courseService.selectCourseStudentByCouNum(page);
        if (course != null) {
            page.setData(course.getUserList());
        }
        page.setCourse(courseService.courseByCouNum(page.getScore().getCourseNum()));
        User user = userService.selectByNumber(page.getUser());
        page.getCourse().setUser(user);
        modelAndView.addObject("page", page);
        modelAndView.setViewName("student/coursedetail");
        return modelAndView;
    }

    /**
     * 根据课程号查询选择课程的学生和学生成绩以及课程信息
     *
     * @param page 接口调用须为必要属性赋值：score.courseNum、pageNumber、size
     * @return ModelAndView
     * @author Fcant
     * @date 上午 8:51 2019-08-11/0011
     */
    @RequestMapping("/cs")
    public ModelAndView selectListStuCourseScoreByCouNum(Page page) {
        ModelAndView modelAndView = new ModelAndView();
        page = FcantUtils.setPageManager(page);
        page.setTotal(courseService.selectCourseStudentByCouNumTotal(page.getScore().getCourseNum()));
        Course course = courseService.selectCourseStudentByCouNum(page);
        if (course != null) {
            page.setData(course.getUserList());
        }
        page.setCourse(courseService.courseByCouNum(page.getScore().getCourseNum()));
        modelAndView.addObject("page", page);
        modelAndView.setViewName("teacher/coursestu");
        return modelAndView;
    }

    /**
     * 根据学号和课程号查询课程详情
     *
     * @param score 接口调用须为必要属性赋值：stuNum、courseNum
     * @return ModelAndView
     * @author Fcant
     * @date 上午 10:51 2019-08-11/0011
     */
    @RequestMapping("/detail")
    public ModelAndView courseScoreDetail(Score score) {
        ModelAndView modelAndView = new ModelAndView();
        Course course = courseService.courseByCouNumUserNum(score);
        User user = new User();
        user.setNumber(score.getStuNum());
        User selectByNumber = userService.selectByNumber(user);
        modelAndView.addObject("stu", selectByNumber);
        modelAndView.addObject("course", course);
        modelAndView.setViewName("student/scoredetail");
        return modelAndView;
    }

    /**
     * 选课接口设计
     *      此处需要设置选课时间为变量赋值
     *
     * @param score 接口调用须为必要属性赋值：stuNum、courseNum
     * @return ModelAndView
     * @author Fcscanf
     * @date 下午 12:44 2019-08-10/0010
     */
    @ResponseBody
    @RequestMapping("/add")
    public MsgUtil addCourse(Score score) throws ParseException {
        MsgUtil msgUtil = new MsgUtil();
        if (scoreService.insert(score) == 1) {
            msgUtil.setCode(200);
            msgUtil.setMsg("选课成功！");
        } else {
            msgUtil.setCode(100);
            msgUtil.setMsg("您已选过该课程，请勿重复选课！");
        }
        return msgUtil;
    }


    /**
     * 查询所有课程
     *
     * @param page 接口调用须为必要属性赋值：pageNumber、size
     * @return ModelAndView
     * @author Fcscanf
     * @date 下午 12:34 2019-08-10/0010
     */
    @RequestMapping("/all")
    public ModelAndView allCourse(Page page) {
        ModelAndView modelAndView = new ModelAndView();
        page.setTotal(courseService.allCourse().size());
        page = FcantUtils.setPageManager(page);
        page.setData(courseService.allCoursePage(page));
        User user = userService.selectByNumber(page.getUser());
        page.setUser(user);
        modelAndView.addObject("page", page);
        modelAndView.setViewName("course/course");
        return modelAndView;
    }

    /**
     * 退课接口
     *
     * @param score 接口调用须为必要属性赋值：stuNum、courseNum
     * @return int
     * @author Fcscanf
     * @date 下午 12:41 2019-08-10/0010
     */
    @ResponseBody
    @RequestMapping("/del")
    public int deleteCourse(Score score) {
        return scoreService.delStuCourse(score);
    }
}
