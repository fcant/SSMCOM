package com.fcant.ssmcom.service;

import com.fcant.ssmcom.bean.Page;
import com.fcant.ssmcom.bean.User;

import java.util.List;

/**
 * UserService
 * <p>
 * encoding:UTF-8
 *
 * @author Fcscanf
 * @description
 * @date 下午 19:36 2019-08-06/0006
 */
public interface UserService {

    /**
     * 模糊查询用户信息
     *
     * @param user 接口调用须为必要属性赋值：rId
     * @return List<User>
     * @author Fcscanf
     * @date 下午 14:19 2019-08-08/0008
     */
    List<User> queryUserLike(User user);

    /**
     * 获取该表的总条目数
     *
     * @return int
     * @author Fcscanf
     * @date 下午 17:16 2019-08-08/0008
     */
    public int tableTotal();

    /**
     * 模糊并分页查询用户信息
     *
     * @param page 接口调用须为必要属性赋值：start、end、user.rId
     * @return List<User>
     * @author Fcscanf
     * @date 下午 14:19 2019-08-08/0008
     */
    List<User> queryUserLikePage(Page page);

    /**
     * 根据学号/工号修改用户信息
     *
     * @param user 接口调用须为必要属性赋值：number
     * @return int
     * @author Fcscanf
     * @date 下午 23:34 2019-08-07/0007
     */
    int updateByNumber(User user);

    /**
     * 添加用户
     *
     * @param user  接口调用须为必要属性赋值：number
     * @return int
     * @author Fcscanf
     * @date 下午 19:37 2019-08-06/0006
     */
    public int addUser(User user);

    /**
     * 根据学号或者工号查询用户
     *
     * @param user 接口调用须为必要属性赋值：number
     * @return User
     * @author Fcscanf
     * @date 上午 9:20 2019-08-07/0007
     */
    User selectByNumber(User user);
}
