<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.fcant.ssmcom.bean.User" %><%--
  Created by IntelliJ IDEA.
  User: fcsca
  Date: 2019-08-03/0003
  Time: 下午 17:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <link href="<%=basePath%>bootstrap-4.1.3-dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="<%=basePath%>jquery-3.3.1/jquery-3.3.1.min.js"></script>
    <script src="<%=basePath%>js/page.js"></script>
    <base href="<%=basePath%>">
    <title>LKD系统学生页面</title>
</head>
<body background="<%=basePath%>img/47.gif">

<div align="right" style="margin-right: 10px">
    <%
        User user = (User) request.getSession().getAttribute("user");
    %>
    欢迎学生<%=user.getName()%>登录
    <a href="<%=basePath%>user/detail?number=<%=user.getNumber()%>">查看个人信息</a>|
    <a href="<%=basePath%>user/signOut">Sign Out</a>
</div>

<div align="center" class="alert alert-success">
    <h3>LKD系统学生页面</h3>
</div>

<%--功能框设计--%>
<div class="alert alert-primary" align="center" style="margin-top: -15px">
    <h4>本学期已选课程</h4>
    <div align="right">
        <a onclick="pageQuery('<%=basePath%>course/all', 1, 'user.number=${page.user.number}')">选课</a>
    </div>
</div>

<%--课程内容展示--%>
<div align="center">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover">
                <tr>
                    <th>课程号</th>
                    <th>课程名</th>
                    <th>教室</th>
                    <th>上课时间</th>
                    <th>结束时间</th>
                    <th>操作</th>
                </tr>
                <c:forEach items="${page.data}" var="course">
                    <tr>
                        <th>${course.num}</th>
                        <th>${course.name}</th>
                        <th>${course.room}</th>
                        <th>${course.startTime}</th>
                        <th>${course.endTime}</th>
                        <th>
                            <button class="btn btn-danger btn-sm" onclick="delCourse('<%=basePath%>', 'sc', ${page.pageNumber}, '<%=user.getNumber()%>', ${course.num}, '${course.name}')">
                                退选课程<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                            </button>
                            <button class="btn btn-success btn-sm" onclick="sendRequest('<%=basePath%>course/detail', 'stuNum=${page.user.number}&courseNum=${course.num}')">
                                查看课程<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                            </button>
                        </th>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
</div>

<%--显示分页的信息--%>
<div class="row">
    <%--分页文字信息--%>
    <div class="col-md-7">当前第${page.pageNumber}页|共有${page.setTolPageNum()}页|共有${page.total}条记录
    </div>
    <%--分页条信息--%>
    <div class="col-md-5">
        <div align="right">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item">
                        每页显示<input id="size" style="width: 10%;" value="${page.size}">条数据
                    </li>
                    <li class="page-item">
                        <a class="page-link" aria-label="Previous">
                            <span aria-hidden="true" onclick="pageQuery('<%=basePath%>user/sc', 1, 'user.number=<%=user.getNumber()%>')">首页</span>
                        </a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" onclick="pageQueryPro('<%=basePath%>user/sc', ${page.pageNumber}, 'user.number=<%=user.getNumber()%>')" aria-label="Previous">
                            <span aria-hidden="true" onclick="">&laquo;</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" onclick="pageQuery('<%=basePath%>user/sc', ${page.pageNumber}, 'user.number=<%=user.getNumber()%>')">${page.pageNumber}</a></li>
                    <%--                    <li class="page-item"><a class="page-link" href="#">2</a></li>--%>
                    <%--                    <li class="page-item"><a class="page-link" href="#">3</a></li>--%>
                    <li class="page-item">
                        <a class="page-link" onclick="pageQueryNext('<%=basePath%>user/sc', ${page.pageNumber}, ${page.setTolPageNum()} , 'user.number=<%=user.getNumber()%>')" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" aria-label="Next">
                            <span aria-hidden="true" onclick="pageQuery('<%=basePath%>user/sc', ${page.setTolPageNum()}, 'user.number=<%=user.getNumber()%>')">尾页</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
</body>
<script>

</script>
</html>
