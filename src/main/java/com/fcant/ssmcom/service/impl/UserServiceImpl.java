package com.fcant.ssmcom.service.impl;

import com.fcant.ssmcom.bean.Page;
import com.fcant.ssmcom.bean.User;
import com.fcant.ssmcom.mapper.UserMapper;
import com.fcant.ssmcom.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Pattern;

/**
 * UserServiceImpl
 * <p>
 * encoding:UTF-8
 *
 * @author Fcscanf
 * @description
 * @date 下午 19:37 2019-08-06/0006
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    /**
     * 模糊查询用户信息
     *
     * @param user 接口调用须为必要属性赋值：rId
     * @return List<User>
     * @author Fcscanf
     * @date 下午 14:19 2019-08-08/0008
     */
    @Override
    public List<User> queryUserLike(User user) {
        return userMapper.queryUserLike(user);
    }

    /**
     * 获取该表的总条目数
     *
     * @return int
     * @author Fcscanf
     * @date 下午 17:16 2019-08-08/0008
     */
    @Override
    public int tableTotal() {
        return userMapper.tableTotal();
    }

    /**
     * 模糊并分页查询用户信息
     *
     * @param page 接口调用须为必要属性赋值：start、end、user.rId
     * @return List<User>
     * @author Fcscanf
     * @date 下午 14:19 2019-08-08/0008
     */
    @Override
    public List<User> queryUserLikePage(Page page) {
        return userMapper.queryUserLikePage(page);
    }

    /**
     * 根据学号/工号修改用户信息
     *
     * @param user 接口调用须为必要属性赋值：number
     * @return int
     * @author Fcscanf
     * @date 下午 23:34 2019-08-07/0007
     */
    @Override
    public int updateByNumber(User user) {
        if (checkPhone(user.getTel())) {
            if (checkEmail(user.getEmail())) {
                return userMapper.updateByNumber(user);
            }
            return 4;
        }
        return 5;

    }

    /**
     * 添加用户
     *
     * @param user 接口调用须为必要属性赋值：number
     * @return int
     * @author Fcscanf
     * @date 下午 19:37 2019-08-06/0006
     */
    @Override
    public int addUser(User user) {
        User byNumber = userMapper.selectByNumber(user);
        if (byNumber != null) {
            return 6;
        }else {
            if (checkPhone(user.getTel())) {
                if (checkEmail(user.getEmail())) {
                    user.setIsvalid(1);
                    return userMapper.addUser(user);
                }else {
                    return 4;
                }
            }else {
                return 5;
            }
        }
    }

    /**
     * 根据学号或者工号查询用户
     *
     * @param user 接口调用须为必要属性赋值：number
     * @return User
     * @author Fcscanf
     * @date 上午 9:20 2019-08-07/0007
     */
    @Override
    public User selectByNumber(User user) {
        User selectByNumber = userMapper.selectByNumber(user);
        return selectByNumber;
    }

    /**
     * 校验邮箱
     *
     * @param email 邮箱
     * @return true
     * 校验通过返回true
     * 校验失败返回false
     * @author Fcscanf
     * @date 下午 17:10 2019-07-22/0022
     */
    public boolean checkEmail(String email) {
        return Pattern.matches("^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$" , email);
    }

    /**
     * 校验手机号
     *
     * @param phone 手机号
     * @return true
     * @author Fcscanf
     * @date 下午 17:14 2019-07-22/0022
     */
    public boolean checkPhone(String phone) {
        return Pattern.matches("^((1[0-9][0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$" , phone);
    }
}
