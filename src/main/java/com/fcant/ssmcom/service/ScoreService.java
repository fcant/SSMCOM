package com.fcant.ssmcom.service;

import com.fcant.ssmcom.bean.Course;
import com.fcant.ssmcom.bean.Score;
import com.fcant.ssmcom.bean.User;

import java.text.ParseException;
import java.util.List;

/**
 * ScoreService
 * <p>
 * encoding:UTF-8
 *
 * @author Fcscanf
 * @date 下午 14:06 2019-08-09/0009
 */
public interface ScoreService {

    /**
     * 添加成绩
     *
     * @param score 接口调用时为必要属性赋值：scId、score
     * @return int
     * @author Fcant
     * @date 上午 8:12 2019-08-12/0012
     */
    int updateByPrimaryKey(Score score);

    /**
     * 根据课程号和学号查询当前选课是否存在
     *
     * @param score 调用接口时须为必要属性赋值：stuNum、courseNum
     * @return Score
     * @author Fcant
     * @date 下午 23:46 2019-08-11/0011
     */
    Score selectCourseByCouNumStuNum(Score score);

    /**
     * 选课
     *
     * @param score 接口调用须为必要属性赋值：stuNum、courseNum
     * @return int
     * @author Fcscanf
     * @date 下午 12:33 2019-08-10/0010
     */
    int insert(Score score) throws ParseException;

    /**
     * 退课
     *
     * @param score 接口调用须为必要属性赋值：stuNum、courseNum
     * @return int
     * @author Fcscanf
     * @date 下午 20:21 2019-08-09/0009
     */
    int delStuCourse(Score score);

    /**
     * 根据课程号查询学号
     *
     * @param course 接口调用须为必要属性赋值：num
     * @return List<Score>
     * @author Fcscanf
     * @date 下午 13:58 2019-08-09/0009
     */
    List<Score> queryByCourseNum(Course course);

    /**
     * 根据学号查询选课成绩表
     *
     * @param user 接口调用须为必要属性赋值：number
     * @return List<Score>
     * @author Fcscanf
     * @date 下午 13:56 2019-08-09/0009
     */
    List<Score> queryByNumber(User user);
}
